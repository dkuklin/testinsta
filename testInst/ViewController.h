//
//  ViewController.h
//  testInst
//
//  Created by DKuklin on 28.08.14.
//  Copyright (c) 2014 self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *instaNameSearch;
@property (weak, nonatomic) IBOutlet UIButton *getCollage;
- (IBAction)getCollagePressed:(id)sender;
- (IBAction)textFieldHideKeyboard:(id)sender;

@end
