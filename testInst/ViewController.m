//
//  ViewController.m
//  testInst
//
//  Created by DKuklin on 28.08.14.
//  Copyright (c) 2014 self. All rights reserved.
//

#import "ViewController.h"
#import "CollageViewController.h"

#define FIRSTELEMENT 0
#define SECONDELEMENT 1

@interface ViewController (){
    int firstPictureLikes;
    int secondPictureLikes;
    long firstPictureLikesCount;
    long secondPictureLikesCount;
    NSMutableArray *bestPictures;
}

@end

#pragma mark - controller methods

@implementation ViewController
@synthesize instaNameSearch;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"toImage"]) {
        CollageViewController *collageVC = segue.destinationViewController;
        collageVC.firstImage = [bestPictures objectAtIndex:0];
        collageVC.secondImage = [bestPictures objectAtIndex:1];
        collageVC.firstImageLikeCount = firstPictureLikesCount;
        collageVC.secondImageLikeCount = secondPictureLikesCount;
    }
}

#pragma mark - actions

- (IBAction)getCollagePressed:(id)sender {
    if (![instaNameSearch.text isEqualToString:@""] && (instaNameSearch.text != nil)) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.instagram.com/v1/users/search?q=%@&client_id=6dcdb17735014449978467c2eb289891", instaNameSearch.text]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                           timeoutInterval:100];
        
        [request setHTTPMethod: @"GET"];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error == nil) {
                 NSString * text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                 NSLog(@"responce %@", text);
                 
                 NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 if (error == nil) {
                     NSArray* user = [json objectForKey:@"data"];
                     if (user.count > 0 && user != nil) {
                         NSArray *userId = [user valueForKey:@"id"];
                         NSString *idUser = [userId objectAtIndex:0];
                         
                         
                         NSURL *url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.instagram.com/v1/users/%@/media/recent/?count=100&client_id=6dcdb17735014449978467c2eb289891", idUser]];
                         [self getImageWith:url1];
                     } else {
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"The user unavailable" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                         [alert show];
                     }
                 }
             } else {
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", error] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 [alert show];
                 NSLog(@"%@",[error localizedDescription]);
             }
         }];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Type a nickname" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)textFieldHideKeyboard:(id)sender {
    [instaNameSearch resignFirstResponder];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (void)getImageWith:(NSURL *)url{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:100];
    
    [request setHTTPMethod: @"GET"];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error == nil) {
             NSString * text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             NSLog(@"responce %@", text);
             
             NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             if (error == nil) {
                 NSArray* user = [json objectForKey:@"data"];
                 if (user != nil) {
                     NSArray* likes = [[user valueForKey:@"likes"] valueForKey:@"count"];
                     
                     firstPictureLikes = 0;
                     secondPictureLikes = 0;
                     
                     for (int i = 0; i < likes.count; i++) {
                         NSLog(@"%d, %d", [[likes objectAtIndex:firstPictureLikes] intValue], [[likes objectAtIndex:i] intValue]);
                         if ([[likes objectAtIndex:firstPictureLikes] longValue] <= [[likes objectAtIndex:i] longValue]) {
                             secondPictureLikes = firstPictureLikes;
                             firstPictureLikes = i;
                         } else {
                             if (firstPictureLikes == secondPictureLikes) {
                                 secondPictureLikes = i;
                             } else if([[likes objectAtIndex:secondPictureLikes] longValue] < [[likes objectAtIndex:i] longValue]){
                                 secondPictureLikes = i;
                             }
                         }
                     }
                     
                     firstPictureLikesCount = [[likes objectAtIndex:firstPictureLikes] longValue];
                     secondPictureLikesCount = [[likes objectAtIndex:secondPictureLikes] longValue];
                     
                     NSArray *array = [[[user valueForKey:@"images"] valueForKey:@"standard_resolution"] valueForKey:@"url"];
                     
                     bestPictures = [[NSMutableArray alloc] init];
                     [bestPictures addObject:[array objectAtIndex:firstPictureLikes]];
                     [bestPictures addObject:[array objectAtIndex:secondPictureLikes]];
                     
                     [self performSegueWithIdentifier:@"toImage" sender:self];
                 } else {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"The user unavailable" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alert show];
                 }
                 
             }
         } else {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", error] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [alert show];
             NSLog(@"%@",[error localizedDescription]);
         }
     }];
    
}
@end
