//
//  main.m
//  testInst
//
//  Created by DKuklin on 28.08.14.
//  Copyright (c) 2014 self. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
