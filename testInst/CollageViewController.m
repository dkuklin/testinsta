//
//  CollageViewController.m
//  testInst
//
//  Created by DKuklin on 28.08.14.
//  Copyright (c) 2014 self. All rights reserved.
//

#import "CollageViewController.h"

@interface CollageViewController ()

@end

@implementation CollageViewController

@synthesize firstImage, secondImage, resultImage, firstLikesCount, secondLikesCount, firstImageLikeCount, secondImageLikeCount;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", firstImage]];
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    UIImage *image = [UIImage imageWithData:imageData];
    
    imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", secondImage]];
    imageData = [NSData dataWithContentsOfURL:imageURL];
    UIImage *image1 = [UIImage imageWithData:imageData];
    
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(70, 0, 500, 320)]; //TODO: delete magic numbers
    [image1 drawInRect:CGRectMake(70, 330, 500, 320)];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [resultImage setImage:result];
    
    firstLikesCount.text = [NSString stringWithFormat:@"LIKES: %li", firstImageLikeCount];
    secondLikesCount.text = [NSString stringWithFormat:@"LIKES: %li", secondImageLikeCount];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)printItPressed:(id)sender {
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, YES, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.view.layer renderInContext:context];
    UIImage *imageFromCurrentView = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIPrintInteractionController *printController = [UIPrintInteractionController sharedPrintController];
    printController.printingItem = imageFromCurrentView;
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGrayscale;
    printController.printInfo = printInfo;
    printController.showsPageRange = YES;
    
    
    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
        if (!completed && error) {
            NSLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
        }
    };
    
    [printController presentAnimated:YES completionHandler:completionHandler];

}
@end
