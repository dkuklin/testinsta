//
//  CollageViewController.h
//  testInst
//
//  Created by DKuklin on 28.08.14.
//  Copyright (c) 2014 self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *resultImage;
@property (weak, nonatomic) IBOutlet UIButton *printIt;
@property (nonatomic) NSString *firstImage;
@property (nonatomic) NSString *secondImage;

@property long firstImageLikeCount;
@property long secondImageLikeCount;

- (IBAction)printItPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *firstLikesCount;
@property (weak, nonatomic) IBOutlet UILabel *secondLikesCount;

@end
